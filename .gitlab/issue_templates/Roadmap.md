<Year> <Month> - Roadmapping

## Goal

Update the roadmap:

- prioritize our topics
- provide a guesstimate on the topics

The guess should be concrete with specific epics at least (even better with scoped issues) for the next 3 months, and can just mention the quarters for the whole year.

<!-- The roadmap should be built up from potentially multiple development tracks -->

We want to provide a roadmap for the following product categories:

- Infrastructure as Code
- Kubernetes Management

## Links

[Mural board showing the prioritized topics](https://app.mural.co/t/gitlab2474/m/gitlab2474/1603196180832/a0fbe8a5eb174a4b5dda15b44728ecbc7a4b377d)

